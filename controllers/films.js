var express = require('express')
  , router = express.Router()
  , axios = require('axios')
  , Film = require('../models/Film')
  , Character = require('../models/Character')
  , Planet = require('../models/Planet')
  , Vehicle = require('../models/Vehicle');

const instance = axios.create({
  baseURL: 'https://swapi.co/api'
});

router.get('/:film', function(req, res) {
  let id = req.params.film;
  Film.get(id).then((response) => {

    requests = [];
    characterResults = [];
    planetResults = [];
    vehicleResults = [];

    for (character of response.data.characters) {
      requests.push(Character.getName(character).then((result) => characterResults.push(result)));
    }

    for (planet of response.data.planets) {
      requests.push(Planet.getName(planet).then((result) => planetResults.push(result)));
    }

    for (vehicle of response.data.vehicles) {
      requests.push(Vehicle.getName(vehicle).then((result) => vehicleResults.push(result)));
    }

    axios.all(requests).then((results) => {
      res.render('film', {json: response.data, characterData: characterResults, planetData: planetResults, vehicleData: vehicleResults});
    }).catch((e) => {
      res.status(400).render('error', {error: e})});
  }).catch((e) => {
    res.status(400).render('error', {error: e})
    });
});

module.exports = router