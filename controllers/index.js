const router = require('express').Router();
const Film = require('../models/Film');

router.use('/characters', require('./characters'));
router.use('/films', require('./films'));
router.use('/vehicles', require('./vehicles'));
router.use('/planets', require('./planets'));

router.get('/', (req, res) => {
	Film.all().then((response) => {
    response.data.results.map((x) => {
      x.id = Film.getId(x.url);
    });
		res.render('index', {json: response.data});
  }).catch((e) => {
    console.log(e)
    res.status(400).render('error', {error:e});
  });
});

router.get('/search', (req, res) => {
  if (!req.query.hasOwnProperty('title')) {
    res.status(400).render('error', {error: 'Query missing title'});
  }
  let title = req.query.title;
  Film.search(title).then((response) => {
    response.data.results.map((x) => {
      x.id = Film.getId(x.url);
    });
    res.render('index', {json: response.data});
  }).catch((e) => {
    res.status(400).render('error', {error:e});
  });
});

router.get('*', (req, res) => {
	res.status(404).render('404');
});

module.exports = router;