var express = require('express')
  , router = express.Router()
  , Planet = require('../models/Planet');

router.get('/:id', function(req, res) {
  Planet.get(req.params.id).then((response) => {
    res.render('planet', {json: response.data});
  }).catch((e) => res.status(400).render('error', {error: e}));
});


module.exports = router