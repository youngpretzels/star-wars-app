var express = require('express')
  , router = express.Router()
  , Vehicle = require('../models/Vehicle');

router.get('/:id', function(req, res) {
  Vehicle.get(req.params.id).then((response) => {
    res.render('vehicle', {json: response.data});
  }).catch((e) => res.status(400).render('error', {error: e}));
});

module.exports = router