var express = require('express')
  , router = express.Router()
  , Character = require('../models/Character');

router.get('/:id', function(req, res) {
  Character.get(req.params.id).then((response) => {
    res.render('character', {json: response.data});
  }).catch((e) => res.status(400).render('error', {error: e}));
});

module.exports = router