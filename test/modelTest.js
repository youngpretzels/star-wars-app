let chai = require('chai');
let chaiHttp = require('chai-http');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiHttp);
chai.use(chaiAsPromised);
const expect = chai.expect;

var Film = require('../models/Film')
  , Character = require('../models/Character')
  , Planet = require('../models/Planet')
  , Vehicle = require('../models/Vehicle');

describe('Testing data validation', () => {

	let validFilmProperties = {'title': 'Film', 'release_date': '19-05-1990', 'opening_crawl': 'Description', 
      'characters': ['https://swapi.co/api/people/3/'], 'planets': ['https://swapi.co/api/planets/3/'], 
      'vehicles': 'https://swapi.co/api/vehicles/6/' };

    let invalidFilmProperties = {'title': 'Film', 'release_date': '19-05-1990', 'opening_crawl': 'Description', 
      'characters': ['https://swapi.co/api/people/3/'], 
      'vehicles': 'https://swapi.co/api/vehicles/6/' };

    let validCharProperties = {'name': 'Film', 'birth_year': '2000', 'gender': 'Unknown', 'height': '164', 
      'mass': '800', 'hair_color': 'green', 'skin_color': 'green', 'eye_color': 'green' };

    let invalidCharProperties = {'name': 'Film', 'birth_year': '2000', 'gender': 'Unknown', 'height': '164', 
      'hair_color': 'green', 'sknn_color': 'green', 'eye_color': 'green' };

    let validFilmResponse = {'data': validFilmProperties};
    let missingDataFilmResponse = {'dt': validFilmProperties};
    let invalidFilmResponse = {'data': invalidFilmProperties};
    let validCharResponse = {'data': validCharProperties};
    let invalidCharResponse = {'data': invalidCharProperties};

    let validFilmResultsResponse = {'data': {'results': [validFilmProperties, validFilmProperties]}};
    let missingFilmResultsResponse = {'data': [validFilmProperties, validFilmProperties]};
    let invalidFilmResultsResponse = {'data': {'results': [invalidFilmProperties, validFilmProperties]}};



    it('valid film data should pass', () => {
    	expect(() => Film.validateData(validFilmResponse)).not.to.throw();
    }); 

    it('valid character data should pass', () => {
    	expect(() => Character.validateData(validCharResponse)).not.to.throw();
    });

    it('responses with no data should throw an error', () => {
    	expect(() => Film.validateData(missingDataFilmResponse)).to.throw();
    });

    it('responses with missing film data should throw an error', () => {
    	expect(() => Film.validateData(invalidFilmResponse)).to.throw();
    });

    it('responses with missing character data should throw an error', () => {
    	expect(() => Character.validateData(invalidCharResponse)).to.throw();
    });

    it('valid film data with multiple results should pass', () => {
    	expect(() => Film.validateData(validFilmResultsResponse)).not.to.throw();
    }); 

    it('film data with missing results should pass', () => {
    	expect(() => Film.validateData(missingFilmResultsResponse)).to.throw();
    }); 

    it('film data with invalid results should pass', () => {
    	expect(() => Film.validateData(invalidFilmResultsResponse)).to.throw();
    }); 
});

describe('Testing URL validation', () => {

	let validFilmURL = 'https://swapi.co/api/films/2';

    it( 'film url should pass for film', () => {
    	expect(() => Film.validateURL(validFilmURL)).not.to.throw();
    }); 

    it( 'film url should throw an error for planets', () => {
    	expect(() => Planet.validateURL(validFilmURL)).to.throw();
    }); 
});


describe('Testing getId', () => {

	let validId = 1;
	let invalidId = 999; 

	let validIdURL = 'https://swapi.co/api/people/2/';
	let invalidIdURL = 'people2';

    it('should return id from valid url', () => {
        let id = Film.getId(validIdURL);
        expect(id).to.equal('2');
    });

    it('should throw an error for an invalid url', () => {
        expect(() => Film.getId(invalidIdURL)).to.throw();
    });


	it('all function should return a valid response', async () => {
        let response = await Film.all();
	    expect(response).to.have.status(200);
	    expect(response).to.have.property('data');
	    expect(response.data).to.have.property('results');
    });
});

describe('Testing getName', () => {

	let validURL = 'https://swapi.co/api/people/2/';
	let expectedName = 'C-3PO';
	let expectedId = '2';
	let invalidURL = 'https://swapi.co/api/people/666/';

    it('should return name from valid url', async () => {
        let data = await Character.getName(validURL);
        expect(data.name).to.equal(expectedName);
        expect(data.id).to.equal(expectedId);
    });

    it('should throw an error for an invalid url', async () => {
        await expect(Character.getName(invalidURL)).to.eventually.be.rejectedWith(Error);

    });


	it('all function should return a valid response', async () => {
        let response = await Film.all();
	    expect(response).to.have.status(200);
	    expect(response).to.have.property('data');
	    expect(response.data).to.have.property('results');
    });
});


describe('Testing getInfo', () => {

	let validId = 4;
	let invalidId = 999;

    it('getInfo should return a valid response to a valid id - planet', async () => {
    	let response = await Planet.getInfo(validId);
    	expect(response).to.have.status(200);
	    expect(response).to.have.property('data');
    });

    it('getInfo should return a valid response to a valid id - vehicle', async () => {
    	let response = await Vehicle.getInfo(validId);
    	expect(response).to.have.status(200);
	    expect(response).to.have.property('data');
    });

    it('getInfo should throw an error for an invalid id - film', async () => {
    	await expect(Film.getInfo(invalidId)).to.eventually.be.rejectedWith(Error);
    });

	it('all function should return a valid response', async () => {
        let response = await Film.all();
	    expect(response).to.have.status(200);
	    expect(response).to.have.property('data');
	    expect(response.data).to.have.property('results');
    });
});

describe('Testing search', () => {

	let searchTitle = 're';
	let searchTitleNoResults = 'fffffff';

    it('search should return a valid set of results to a valid query', async () => {
    	let response = await Film.search(searchTitle);
    	expect(response).to.have.status(200);
	    expect(response).to.have.property('data');
	    expect(response.data).to.have.property('results');
	    expect(response.data.results).to.have.length.above(0);

    });

    it('search should return an empty set of results to a query with no match', async () => {
    	let response = await Film.search(searchTitleNoResults);
    	expect(response).to.have.status(200);
	    expect(response).to.have.property('data');
	    expect(response.data).to.have.property('results').that.eql([]);
    });

})