let chai = require('chai');
let chaiHttp = require('chai-http');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiHttp);
chai.use(chaiAsPromised);
const expect = chai.expect;
let app;

var Film = require('../models/Film')
  , Character = require('../models/Character')
  , Planet = require('../models/Planet')
  , Vehicle = require('../models/Vehicle');

describe('Controller tests', () => {

	beforeEach(() => {
  		app = require('../app.js');
	});

	afterEach(function () {
		app.close();
	});

	describe('Testing index controllers', () => {

		it('index page should render', async () => {
			let response = await chai.request(app).get('/');
		    expect(response).to.have.status(200);
		});

	    it('valid search request should render', async () => {
			let response = await chai.request(app).get('/search?title=r');
		    expect(response).to.have.status(200);
		});

		it('search request with an empty query should render', async () => {
			let response = await chai.request(app).get('/search?title=');
		    expect(response).to.have.status(200);
		});

	    it('invalid search request should give an error', async () => {
			let response = await chai.request(app).get('/search?canoe=');
		    expect(response).to.have.status(400);
		});

	});

	describe('Testing film controllers', () => {

		it('film page with valid id should render', async () => {
			let response = await chai.request(app).get('/films/2');
		    expect(response).to.have.status(200);
		});

	    it('film page with invalid id should throw error', async () => {
			let response = await chai.request(app).get('/films/999');
		    expect(response).to.have.status(400);
		});

	});

    describe('Testing character controllers', () => {

		it('character page with valid id should render', async () => {
			let response = await chai.request(app).get('/characters/2');
			console.log(response)
		    expect(response).to.have.status(200);
		});

	    it('character page with invalid id should throw error', async () => {
			let response = await chai.request(app).get('/characters/999');
		    expect(response).to.have.status(400);
		});

	});

	describe('Testing planet controllers', () => {

		it('planet page with valid id should render', async () => {
			let response = await chai.request(app).get('/planets/2');
		    expect(response).to.have.status(200);
		});

	    it('planet page with invalid id should throw error', async () => {
			let response = await chai.request(app).get('/planets/999');
		    expect(response).to.have.status(400);
		});

	});

	describe('Testing vehicle controllers', () => {

		it('vehicle page with valid id should render', async () => {
			let response = await chai.request(app).get('/vehicles/4');
		    expect(response).to.have.status(200);
		});

	    it('vehicle page with invalid id should throw error', async () => {
			let response = await chai.request(app).get('/vehicles/999');
		    expect(response).to.have.status(400);
		});

	});

    describe('Testing 404s', () => {

		it('nonexistent page from index', async () => {
			let response = await chai.request(app).get('/flims');
		    expect(response).to.have.status(404);
		});

	    it('nonexistent page from films', async () => {
			let response = await chai.request(app).get('/films/character/characters');
		    expect(response).to.have.status(404);
		});
	});



});