
const path = require("path");
const express = require("express");
const axios = require('axios');

const app = express();
const routes = require('./controllers');
const port = process.env.PORT || "8000";

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static(path.join(__dirname, "public")));
app.use('/', routes);


var server = app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});

module.exports = server;