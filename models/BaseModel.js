  const axios = require('axios');
  const instance = axios.create({
    baseURL: 'https://swapi.co/api'
  });

class BaseModel {

  constructor(path, properties) {
    this.path = path;
    this.properties = properties;
  }

  async all() {
    try {
      const response = await this.getInfo();
      return response;
    } catch (error) {
      throw(error);
    }
  }

  async get(id) {
  	try {
  	    const response = await this.getInfo(id);
  	    return response;
  	  } catch (error) {
  	    throw(error);
  	  }
  }

  async getInfo(id='') {
    try {
      const response = await instance.get(this.path + id.toString());
      this.validateData(response);
      return response;
    } catch (error) {
      throw(error);
    }
  }

  async getName(url) {
      try {
      this.validateURL(url);
      var id = this.getId(url);
      const response = await this.getInfo(id);
      if (response.data.name != null) {
        return {name: response.data.name, id: this.getId(url)};
      } else {
        throw(new Error('This item has no name'));
      }
    } catch (error) {
      throw(error);
    }
  }

  getId(url) {

    let splitURL = url.split('/');
    if (splitURL.length < 2) {
      throw(new Error('invalid URL'));
    } else {
      let lastItem = splitURL.slice(-2)[0];
      return lastItem;
    } 
}
  async search(title) {
    try {
      const response = await instance.get(this.path + '?search=' + title.toString());
      this.validateData(response);
      return response;
    } catch (error) {
      throw(error);
    }
  }

  validateData(response) {

    if (!response.hasOwnProperty("data")) {
      throw(new Error("Response has no data"));
    }
    const data = response.data;

    if (data.hasOwnProperty("results")) {
      for (let object of data.results) {
        this.validateProperties(object);
      }
    } else {
      this.validateProperties(data);
    }
  }

  validateProperties(object) {
    for (let property of this.properties) {
      if (!object.hasOwnProperty(property)) {
        throw(new Error("Response missing " + property));
      }
    }
  }

  validateURL(url) {
    if (!url.includes(this.path)) {
      throw(new Error("URL doesn't match data type"));
    } 
  }
}

module.exports = BaseModel;