BaseModel = require('./BaseModel');

class Planet extends BaseModel {

  constructor() {
    super('/planets/', ['name', 'rotation_period', 'orbital_period', 'diameter', 'climate', 'gravity', 'terrain']);
  }

}

module.exports = new Planet();


