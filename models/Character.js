BaseModel = require('./BaseModel');

class Character extends BaseModel {

  constructor() {
    super('/people/', ['name', 'birth_year', 'gender', 'height', 
      'mass', 'hair_color', 'skin_color', 'eye_color']);
  }
}

module.exports = new Character();

