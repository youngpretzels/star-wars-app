BaseModel = require('./BaseModel');

class Film extends BaseModel {

	constructor() {
		super('/films/', ['title', 'release_date', 'opening_crawl', 
      'characters', 'planets', 'vehicles']);
	}
}

module.exports = new Film();
