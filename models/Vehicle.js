BaseModel = require('./BaseModel');

class Vehicle extends BaseModel {

  constructor() {
    super('/vehicles/', ['name', 'model', 'manufacturer', 'cost_in_credits', 
    	'length', 'max_atmosphering_speed', 'crew', 'passengers', 'cargo_capacity', 'consumables', 'vehicle_class']);
  }

}

module.exports = new Vehicle();


